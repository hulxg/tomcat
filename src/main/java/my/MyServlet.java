package my;


import javax.servlet.ServletException;
import javax.servlet.SingleThreadModel;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class MyServlet extends HttpServlet  {

	AtomicInteger num = new AtomicInteger(0);
    @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		System.out.println(num.incrementAndGet()+" "+Thread.currentThread().getName());
		/*try{
			Thread.sleep(10000);
		}catch(Exception e){
		    e.printStackTrace();
		}*/
		resp.getWriter().write("hello hulxg");
	}
}