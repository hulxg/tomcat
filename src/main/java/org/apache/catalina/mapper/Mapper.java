/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.apache.catalina.mapper;

import org.apache.catalina.*;
import org.apache.tomcat.util.buf.Ascii;
import org.apache.tomcat.util.buf.CharChunk;
import org.apache.tomcat.util.buf.MessageBytes;
import org.apache.tomcat.util.res.StringManager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Mapper, which implements the servlet API mapping rules (which are derived
 * from the HTTP rules).
 *
 * @author Remy Maucherat
 */
public final class Mapper {


    private static final org.apache.juli.logging.Log log =
        org.apache.juli.logging.LogFactory.getLog(Mapper.class);

    protected static final StringManager sm =
        StringManager.getManager(Mapper.class.getPackage().getName());

    // ----------------------------------------------------- Instance Variables


    /**
     * Array containing the virtual hosts definitions.
     */
    protected MappedHost[] hosts = new MappedHost[0];//对host的mapper信息


    /**
     * Default host name.
     */
    protected String defaultHostName = null;//engine使用的默认的host名字


    /**
     * Mapping from Context object to Context version to support
     * RequestDispatcher mappings.
     */
    //对context的map信息，key是context对象，value是contextVersion对象
    protected Map<Context, ContextVersion> contextObjectToContextVersionMap =
            new ConcurrentHashMap<>();


    // --------------------------------------------------------- Public Methods

    /**
     * Set default host.
     *
     * @param defaultHostName Default host name
     */
    public void setDefaultHostName(String defaultHostName) {
        this.defaultHostName = defaultHostName;
    }

    /**
     * 添加一个新的host
     *
     * @param name Virtual host name
     * @param host Host object
     */
    public synchronized void addHost(String name, String[] aliases,
                                     Host host) {
        MappedHost[] newHosts = new MappedHost[hosts.length + 1];//创建一个新的host数据
        MappedHost newHost = new MappedHost();//创建一个MappedHost对象，用于保存host的map信息
        ContextList contextList = new ContextList();//ContextList对象，用于保存context
        newHost.name = name; //设置名字
        newHost.contextList = contextList;
        newHost.object = host;//设置对应的host对象
        if (insertMap(hosts, newHosts, newHost)) {//这里需要复制以前的host的map信息，并维护好排序
            hosts = newHosts;//指向新的数据
        }
        for (int i = 0; i < aliases.length; i++) { //遍历这个host的所有的别名，为每一个别名都创建一次map信息
            newHosts = new MappedHost[hosts.length + 1];
            newHost = new MappedHost();
            newHost.name = aliases[i];
            newHost.contextList = contextList;//指向同一个contextList
            newHost.object = host;
            if (insertMap(hosts, newHosts, newHost)) {
                hosts = newHosts;
            }
        }
    }


    /**
     * Remove a host from the mapper.
     *
     * @param name Virtual host name
     */
    public synchronized void removeHost(String name) {
        // Find and remove the old host
        int pos = find(hosts, name);
        if (pos < 0) {
            return;
        }
        Host host = hosts[pos].object;
        MappedHost[] newHosts = new MappedHost[hosts.length - 1];
        if (removeMap(hosts, newHosts, name)) {
            hosts = newHosts;
        }
        // Remove all aliases (they will map to the same host object)
        for (int i = 0; i < newHosts.length; i++) {
            if (newHosts[i].object == host) {
                MappedHost[] newHosts2 = new MappedHost[hosts.length - 1];
                if (removeMap(hosts, newHosts2, newHosts[i].name)) {
                    hosts = newHosts2;
                }
            }
        }
    }

    /**
     * Add an alias to an existing host.
     * @param name  The name of the host
     * @param alias The alias to add
     */
    public synchronized void addHostAlias(String name, String alias) {
        int pos = find(hosts, name);
        if (pos < 0) {
            // Should not be adding an alias for a host that doesn't exist but
            // just in case...
            return;
        }
        MappedHost realHost = hosts[pos];

        MappedHost[] newHosts = new MappedHost[hosts.length + 1];
        MappedHost newHost = new MappedHost();
        newHost.name = alias;
        newHost.contextList = realHost.contextList;
        newHost.object = realHost.object;
        if (insertMap(hosts, newHosts, newHost)) {
            hosts = newHosts;
        }
    }

    /**
     * Remove a host alias
     * @param alias The alias to remove
     */
    public synchronized void removeHostAlias(String alias) {
        // Find and remove the alias
        int pos = find(hosts, alias);
        if (pos < 0) {
            return;
        }
        MappedHost[] newHosts = new MappedHost[hosts.length - 1];
        if (removeMap(hosts, newHosts, alias)) {
            hosts = newHosts;
        }

    }


    /**
     * Add a new Context to an existing Host.
     *
     * @param hostName Virtual host name this context belongs to
     * @param host Host object
     * @param path Context path
     * @param version Context version
     * @param context Context object
     * @param welcomeResources Welcome files defined for this context
     * @param resources Static resources of the context
     */

    /**
     *     为host添加context，其实也就是添加context的map信息，
     *     第一个参数是所属的host的名字，
     *     第二个是这个context的path(/examples啥的)，
     *     第三个是版本，第三个是对应的context对象，
     *     最后是root的资源引用
     */
    public void addContextVersion(String hostName, Host host, String path,
            String version, Context context, String[] welcomeResources,
            WebResourceRoot resources) {

        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);//查找host所在的位置
        if( pos <0 ) {//如果不存在，则添加
            addHost(hostName, new String[0], host);
            hosts = this.hosts;
            pos = find(hosts, hostName);//重新获取下标
        }
        if (pos < 0) {
            log.error("No host found: " + hostName);
        }
        MappedHost mappedHost = hosts[pos];//获取mappedHost对象
        if (mappedHost.name.equals(hostName)) {
            int slashCount = slashCount(path);//获取path里面的斜线的数量
            synchronized (mappedHost) {
                MappedContext[] contexts = mappedHost.contextList.contexts;
                //  //记录最大的context的path里面的斜线的数量
                if (slashCount > mappedHost.contextList.nesting) {
                    mappedHost.contextList.nesting = slashCount;
                }
                int pos2 = find(contexts, path);//在context数组里面获取一个位置，这里要么返回相当的path，要么返回左侧的
                if (pos2 < 0 || !path.equals(contexts[pos2].name)) {//如果没有与新加的path相当的情况下
                    MappedContext newContext = new MappedContext();//创建mappedcontext
                    newContext.name = path;
                    MappedContext[] newContexts = new MappedContext[contexts.length + 1];
                    if (insertMap(contexts, newContexts, newContext)) {//更新mappedContext对象的数组
                        mappedHost.contextList.contexts = newContexts;//指向新的数组
                    }
                    pos2 = find(newContexts, path);//获取新的下标
                }

                MappedContext mappedContext = mappedHost.contextList.contexts[pos2];//获取MappedContext对象

                ContextVersion[] contextVersions = mappedContext.versions;//获取版本信息
                ContextVersion[] newContextVersions =
                    new ContextVersion[contextVersions.length + 1];//新的数组
                ContextVersion newContextVersion = new ContextVersion();
                newContextVersion.path = path;//设置path
                newContextVersion.name = version;// 设置version的名字
                newContextVersion.object = context;//保存context对象
                newContextVersion.welcomeResources = welcomeResources;//保存welcomeResources
                newContextVersion.resources = resources;//保存WebResourceRoot对象
                if (insertMap(contextVersions, newContextVersions, newContextVersion)) {//更新mappedContext的contextVersion的数组
                    mappedContext.versions = newContextVersions;
                    contextObjectToContextVersionMap.put(//key是context，value是ContextVersion
                            context, newContextVersion);
                }
            }
        }

    }


    /**
     * Remove a context from an existing host.
     *
     * @param ctxt      The actual context
     * @param hostName  Virtual host name this context belongs to
     * @param path      Context path
     * @param version   Context version
     */
    public void removeContextVersion(Context ctxt, String hostName,
            String path, String version) {

        contextObjectToContextVersionMap.remove(ctxt);

        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);
        if (pos < 0) {
            return;
        }
        MappedHost host = hosts[pos];
        if (host.name.equals(hostName)) {
            synchronized (host) {
                MappedContext[] contexts = host.contextList.contexts;
                if (contexts.length == 0 ){
                    return;
                }

                int pos2 = find(contexts, path);
                if (pos2 < 0 || !path.equals(contexts[pos2].name)) {
                    return;
                }
                MappedContext context = contexts[pos2];

                ContextVersion[] contextVersions = context.versions;
                ContextVersion[] newContextVersions =
                    new ContextVersion[contextVersions.length - 1];
                if (removeMap(contextVersions, newContextVersions, version)) {
                    context.versions = newContextVersions;

                    if (context.versions.length == 0) {
                        // Remove the context
                        MappedContext[] newContexts = new MappedContext[contexts.length -1];
                        if (removeMap(contexts, newContexts, path)) {
                            host.contextList.contexts = newContexts;
                            // Recalculate nesting
                            host.contextList.nesting = 0;
                            for (int i = 0; i < newContexts.length; i++) {
                                int slashCount = slashCount(newContexts[i].name);
                                if (slashCount > host.contextList.nesting) {
                                    host.contextList.nesting = slashCount;
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     *     添加warpper，首先是所属的host的名字，接着是contextpath，接着是context版本，接着是warpper的path，然后是warpper对象
     * @param hostName
     * @param contextPath
     * @param version
     * @param path
     * @param wrapper
     * @param jspWildCard
     * @param resourceOnly
     */
    public void addWrapper(String hostName, String contextPath, String version,
                           String path, Wrapper wrapper, boolean jspWildCard,
                           boolean resourceOnly) {
        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);//根据host的名字，查找mappedHost对象的下标
        if (pos < 0) {
            return;
        }
        MappedHost host = hosts[pos];//获取mapedHost对象
        if (host.name.equals(hostName)) {
            MappedContext[] contexts = host.contextList.contexts;//获取mappedContext的数组
            int pos2 = find(contexts, contextPath); //找到相应的mappedContext的下标
            if (pos2 < 0) {
                log.error("No context found: " + contextPath );
                return;
            }
            MappedContext context = contexts[pos2];//获取所属的mappedContext对象
            if (context.name.equals(contextPath)) {
                ContextVersion[] contextVersions = context.versions; //获取context的版本数组
                int pos3 = find(contextVersions, version);//寻找对一个的version的位置
                if( pos3<0 ) {
                    log.error("No context version found: " + contextPath + " " +
                            version);
                    return;
                }
                ContextVersion contextVersion = contextVersions[pos3];//获取对应的version的map信息
                if (contextVersion.name.equals(version)) {
                    addWrapper(contextVersion, path, wrapper, jspWildCard,
                            resourceOnly);//添加wrapper 对象
                }
            }
        }
    }


    /**
     * Adds a wrapper to the given context.
     *
     * @param context The context to which to add the wrapper
     * @param path Wrapper mapping
     * @param wrapper The Wrapper object
     * @param jspWildCard true if the wrapper corresponds to the JspServlet
     * @param resourceOnly true if this wrapper always expects a physical
     *                     resource to be present (such as a JSP)
     * and the mapping path contains a wildcard; false otherwise
     */
    //将某个wrapper放到某个对应的context，第二个参数是servlet要map的path，后面是wrapper对象，第一个参数是应该加到的contextVersion对象
    protected void addWrapper(ContextVersion context, String path,
            Wrapper wrapper, boolean jspWildCard, boolean resourceOnly) {

        synchronized (context) {
            MappedWrapper newWrapper = new MappedWrapper();//创建一个mappedWrapper对象
            newWrapper.object = wrapper;
            newWrapper.jspWildCard = jspWildCard;
            newWrapper.resourceOnly = resourceOnly;
            if (path.endsWith("/*")) {// 如果path的map是通配符类型的
                // Wildcard wrapper
                newWrapper.name = path.substring(0, path.length() - 2);//将/*去掉
                MappedWrapper[] oldWrappers = context.wildcardWrappers;// 获取通配符匹配的mappedWrapper数组
                MappedWrapper[] newWrappers =
                    new MappedWrapper[oldWrappers.length + 1];
                if (insertMap(oldWrappers, newWrappers, newWrapper)) {
                    context.wildcardWrappers = newWrappers;
                    int slashCount = slashCount(newWrapper.name);//计算斜线数
                    if (slashCount > context.nesting) {
                        context.nesting = slashCount;//更新当前context拥有的servlet的path里面最多的斜线数目
                    }
                }
            } else if (path.startsWith("*.")) {//表示是扩展名的mapper
                // Extension wrapper
                newWrapper.name = path.substring(2);
                MappedWrapper[] oldWrappers = context.extensionWrappers;//获取扩展名匹配的mappedWrapper的数组
                MappedWrapper[] newWrappers =
                    new MappedWrapper[oldWrappers.length + 1];
                if (insertMap(oldWrappers, newWrappers, newWrapper)) {
                    context.extensionWrappers = newWrappers;
                }
            } else if (path.equals("/")) {// 表示是默认的wrapper
                // Default wrapper
                newWrapper.name = "";
                context.defaultWrapper = newWrapper;
            } else { //最后就是精确的map了
                // Exact wrapper
                if (path.length() == 0) {
                    // Special case for the Context Root mapping which is
                    // treated as an exact match
                    newWrapper.name = "/";
                } else {
                    newWrapper.name = path;
                }
                MappedWrapper[] oldWrappers = context.exactWrappers;
                MappedWrapper[] newWrappers =
                    new MappedWrapper[oldWrappers.length + 1];
                if (insertMap(oldWrappers, newWrappers, newWrapper)) {
                    context.exactWrappers = newWrappers;
                }
            }
        }
    }


    /**
     * Remove a wrapper from an existing context.
     *
     * @param hostName Virtual host name this wrapper belongs to
     * @param contextPath Context path this wrapper belongs to
     * @param path Wrapper mapping
     */
    public void removeWrapper
        (String hostName, String contextPath, String version, String path) {
        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);
        if (pos < 0) {
            return;
        }
        MappedHost host = hosts[pos];
        if (host.name.equals(hostName)) {
            MappedContext[] contexts = host.contextList.contexts;
            int pos2 = find(contexts, contextPath);
            if (pos2 < 0) {
                return;
            }
            MappedContext context = contexts[pos2];
            if (context.name.equals(contextPath)) {
                ContextVersion[] contextVersions = context.versions;
                int pos3 = find(contextVersions, version);
                if( pos3<0 ) {
                    return;
                }
                ContextVersion contextVersion = contextVersions[pos3];
                if (contextVersion.name.equals(version)) {
                    removeWrapper(contextVersion, path);
                }
            }
        }
    }

    protected void removeWrapper(ContextVersion context, String path) {

        if (log.isDebugEnabled()) {
            log.debug(sm.getString("mapper.removeWrapper", context.name, path));
        }

        synchronized (context) {
            if (path.endsWith("/*")) {
                // Wildcard wrapper
                String name = path.substring(0, path.length() - 2);
                MappedWrapper[] oldWrappers = context.wildcardWrappers;
                if (oldWrappers.length == 0) {
                    return;
                }
                MappedWrapper[] newWrappers =
                    new MappedWrapper[oldWrappers.length - 1];
                if (removeMap(oldWrappers, newWrappers, name)) {
                    // Recalculate nesting
                    context.nesting = 0;
                    for (int i = 0; i < newWrappers.length; i++) {
                        int slashCount = slashCount(newWrappers[i].name);
                        if (slashCount > context.nesting) {
                            context.nesting = slashCount;
                        }
                    }
                    context.wildcardWrappers = newWrappers;
                }
            } else if (path.startsWith("*.")) {
                // Extension wrapper
                String name = path.substring(2);
                MappedWrapper[] oldWrappers = context.extensionWrappers;
                if (oldWrappers.length == 0) {
                    return;
                }
                MappedWrapper[] newWrappers =
                    new MappedWrapper[oldWrappers.length - 1];
                if (removeMap(oldWrappers, newWrappers, name)) {
                    context.extensionWrappers = newWrappers;
                }
            } else if (path.equals("/")) {
                // Default wrapper
                context.defaultWrapper = null;
            } else {
                // Exact wrapper
                String name;
                if (path.length() == 0) {
                    // Special case for the Context Root mapping which is
                    // treated as an exact match
                    name = "/";
                } else {
                    name = path;
                }
                MappedWrapper[] oldWrappers = context.exactWrappers;
                if (oldWrappers.length == 0) {
                    return;
                }
                MappedWrapper[] newWrappers =
                    new MappedWrapper[oldWrappers.length - 1];
                if (removeMap(oldWrappers, newWrappers, name)) {
                    context.exactWrappers = newWrappers;
                }
            }
        }
    }


    /**
     * Add a welcome file to the given context.
     *
     * @param hostName
     * @param contextPath
     * @param welcomeFile
     */
    public void addWelcomeFile(String hostName, String contextPath,
            String version, String welcomeFile) {
        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);
        if (pos < 0) {
            return;
        }
        MappedHost host = hosts[pos];
        if (host.name.equals(hostName)) {
            MappedContext[] contexts = host.contextList.contexts;
            int pos2 = find(contexts, contextPath);
            if (pos2 < 0) {
                log.error("No context found: " + contextPath );
                return;
            }
            MappedContext context = contexts[pos2];
            if (context.name.equals(contextPath)) {
                ContextVersion[] contextVersions = context.versions;
                int pos3 = find(contextVersions, version);
                if( pos3<0 ) {
                    log.error("No context version found: " + contextPath + " " +
                            version);
                    return;
                }
                ContextVersion contextVersion = contextVersions[pos3];
                if (contextVersion.name.equals(version)) {
                    int len = contextVersion.welcomeResources.length + 1;
                    String[] newWelcomeResources = new String[len];
                    System.arraycopy(contextVersion.welcomeResources, 0,
                            newWelcomeResources, 0, len - 1);
                    newWelcomeResources[len - 1] = welcomeFile;
                    contextVersion.welcomeResources = newWelcomeResources;
                }
            }
        }
    }


    /**
     * Remove a welcome file from the given context.
     *
     * @param hostName
     * @param contextPath
     * @param welcomeFile
     */
    public void removeWelcomeFile(String hostName, String contextPath,
            String version, String welcomeFile) {
        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);
        if (pos < 0) {
            return;
        }
        MappedHost host = hosts[pos];
        if (host.name.equals(hostName)) {
            MappedContext[] contexts = host.contextList.contexts;
            int pos2 = find(contexts, contextPath);
            if (pos2 < 0) {
                log.error("No context found: " + contextPath );
                return;
            }
            MappedContext context = contexts[pos2];
            if (context.name.equals(contextPath)) {
                ContextVersion[] contextVersions = context.versions;
                int pos3 = find(contextVersions, version);
                if( pos3<0 ) {
                    log.error("No context version found: " + contextPath + " " +
                            version);
                    return;
                }
                ContextVersion contextVersion = contextVersions[pos3];
                if (contextVersion.name.equals(version)) {
                    int match = -1;
                    for (int i = 0; i < contextVersion.welcomeResources.length; i++) {
                        if (welcomeFile.equals(contextVersion.welcomeResources[i])) {
                            match = i;
                            break;
                        }
                    }
                    if (match > -1) {
                        int len = contextVersion.welcomeResources.length - 1;
                        String[] newWelcomeResources = new String[len];
                        System.arraycopy(contextVersion.welcomeResources, 0,
                                newWelcomeResources, 0, match);
                        if (match < len) {
                            System.arraycopy(contextVersion.welcomeResources, match + 1,
                                    newWelcomeResources, match, len - match);
                        }
                        contextVersion.welcomeResources = newWelcomeResources;
                    }
                }
            }
        }
    }


    /**
     * Clear the welcome files for the given context.
     *
     * @param hostName
     * @param contextPath
     */
    public void clearWelcomeFiles(String hostName, String contextPath,
            String version) {
        MappedHost[] hosts = this.hosts;
        int pos = find(hosts, hostName);
        if (pos < 0) {
            return;
        }
        MappedHost host = hosts[pos];
        if (host.name.equals(hostName)) {
            MappedContext[] contexts = host.contextList.contexts;
            int pos2 = find(contexts, contextPath);
            if (pos2 < 0) {
                log.error("No context found: " + contextPath );
                return;
            }
            MappedContext context = contexts[pos2];
            if (context.name.equals(contextPath)) {
                ContextVersion[] contextVersions = context.versions;
                int pos3 = find(contextVersions, version);
                if( pos3<0 ) {
                    log.error("No context version found: " + contextPath + " " +
                            version);
                    return;
                }
                ContextVersion contextVersion = contextVersions[pos3];
                if (contextVersion.name.equals(version)) {
                    contextVersion.welcomeResources = new String[0];
                }
            }
        }
    }



    /**
     *
     * 第一个是host的名字，
     * 第二个是请求路径，例如/examples/servlets/servlet/HelloWorldExample，
     * 第三个是版本，默认使用最后的版本，最后是保存map信息的地方
     *
     */
    public void map(MessageBytes host, MessageBytes uri, String version,
                    MappingData mappingData)
        throws Exception {

        //参数处理
        if (host.isNull()) {
            host.getCharChunk().append(defaultHostName);
        }
        host.toChars();
        uri.toChars();

        // 第一个参数是host的名字，第二个参数访问的path，第三个参数是version，最后保存map信息
        internalMap(host.getCharChunk(), uri.getCharChunk(), version,
                mappingData);

    }


    /**
     * Map the specified URI relative to the context,
     * mutating the given mapping data.
     *
     * @param context The actual context
     * @param uri URI
     * @param mappingData This structure will contain the result of the mapping
     *                    operation
     */
    public void map(Context context, MessageBytes uri,
            MappingData mappingData) throws Exception {

        ContextVersion contextVersion =
                contextObjectToContextVersionMap.get(context);
        uri.toChars();
        CharChunk uricc = uri.getCharChunk();
        uricc.setLimit(-1);
        internalMapWrapper(contextVersion, uricc, mappingData);

    }


    // -------------------------------------------------------- Private Methods


    /**
     * 根据给定的url查询对应的mapper信息
     */
    private final void internalMap(CharChunk host, CharChunk uri,
            String version, MappingData mappingData) throws Exception {

        uri.setLimit(-1);

        MappedContext[] contexts = null;
        MappedContext context = null;
        ContextVersion contextVersion = null;

        int nesting = 0;

        // Virtual host mapping
        if (mappingData.host == null) {
            MappedHost[] hosts = this.hosts;//获取当前所有的host
            int pos = findIgnoreCase(hosts, host);//这里找到所属的host的下标
            if ((pos != -1) && (host.equalsIgnoreCase(hosts[pos].name))) {//找到host的情况下
                mappingData.host = hosts[pos].object;//设置host
                contexts = hosts[pos].contextList.contexts;
                nesting = hosts[pos].contextList.nesting;
            } else {// 没有找到host 就用默认的host
                if (defaultHostName == null) {//默认的host为null 则返回
                    return;
                }
                pos = find(hosts, defaultHostName);
                if ((pos != -1) && (defaultHostName.equals(hosts[pos].name))) {
                    mappingData.host = hosts[pos].object;
                    contexts = hosts[pos].contextList.contexts;
                    nesting = hosts[pos].contextList.nesting;
                } else {
                    return;
                }
            }
        }

        // Context mapping
        if (mappingData.context == null && contexts != null) {
            int pos = find(contexts, uri);//查找对应的context
            if (pos == -1) {
                return;
            }

            int lastSlash = -1;
            int uriEnd = uri.getEnd();
            int length = -1;
            boolean found = false;
            while (pos >= 0) { //这里表示找到了可能可用的context
                if (uri.startsWith(contexts[pos].name)) {// 如果path的开头与context的名字相等
                    length = contexts[pos].name.length();//获取context的name的长度
                    if (uri.getLength() == length) {//如果path的长度就等等与context的名字，那么表示path与context的名字相等
                        found = true;
                        break;
                    } else if (uri.startsWithIgnoreCase("/", length)) {// 如果path在context的名字后面就是“/”了，那么也行
                        found = true;
                        break;
                    }
                }

                if (lastSlash == -1) {//到这里表示刚开始找到的context的不满足
                    lastSlash = nthSlash(uri, nesting + 1);
                } else {
                    lastSlash = lastSlash(uri);
                }
                uri.setEnd(lastSlash);
                pos = find(contexts, uri);
            }
            uri.setEnd(uriEnd);//恢复end

            if (!found) {//找不到相应的context
                if (contexts[0].name.equals("")) {//如果有根context，那么就用它，因为有可能就直接访问类似于 www.baidu.com/aa.do?aa=1这种地址，就是根context
                    context = contexts[0];
                }
            } else {
                context = contexts[pos];
            }
            if (context != null) {//设置contextPath
                mappingData.contextPath.setString(context.name);
            }
        }

        if (context != null) {//这里主要是处理context的版本
            ContextVersion[] contextVersions = context.versions;
            int versionCount = contextVersions.length;
            if (versionCount > 1) {
                Context[] contextObjects = new Context[contextVersions.length];
                for (int i = 0; i < contextObjects.length; i++) {
                    contextObjects[i] = contextVersions[i].object;
                }
                mappingData.contexts = contextObjects;
            }

            if (version == null) {
                // Return the latest version
                contextVersion = contextVersions[versionCount - 1];
            } else {
                int pos = find(contextVersions, version);
                if (pos < 0 || !contextVersions[pos].name.equals(version)) {
                    // Return the latest version
                    contextVersion = contextVersions[versionCount - 1];
                } else {
                    contextVersion = contextVersions[pos];
                }
            }
            mappingData.context = contextVersion.object;
        }

         // Wrapper mapping  找到相应的wrapper
        if ((contextVersion != null) && (mappingData.wrapper == null)) {
            internalMapWrapper(contextVersion, uri, mappingData);
        }

    }


    //wrapper的mapping信息，这里的path还带有前面的context的名字
    private final void internalMapWrapper(ContextVersion contextVersion,
                                          CharChunk path,
                                          MappingData mappingData)
        throws Exception {

        int pathOffset = path.getOffset();//当前path的游标
        int pathEnd = path.getEnd();//获取end
        int servletPath = pathOffset;//servlet的path的游标，这里刚开始设置为path的一样
        boolean noServletPath = false;

        int length = contextVersion.path.length();
        if (length != (pathEnd - pathOffset)) {
            servletPath = pathOffset + length;
        } else {
            noServletPath = true;
            path.append('/');
            pathOffset = path.getOffset();
            pathEnd = path.getEnd();
            servletPath = pathOffset+length;
        }

        path.setOffset(servletPath);

        // Rule 1 -- Exact Match
        MappedWrapper[] exactWrappers = contextVersion.exactWrappers;
        internalMapExactWrapper(exactWrappers, path, mappingData);

        // Rule 2 -- Prefix Match
        boolean checkJspWelcomeFiles = false;
        MappedWrapper[] wildcardWrappers = contextVersion.wildcardWrappers;
        if (mappingData.wrapper == null) {
            internalMapWildcardWrapper(wildcardWrappers, contextVersion.nesting,
                                       path, mappingData);
            if (mappingData.wrapper != null && mappingData.jspWildCard) {
                char[] buf = path.getBuffer();
                if (buf[pathEnd - 1] == '/') {
                    /*
                     * Path ending in '/' was mapped to JSP servlet based on
                     * wildcard match (e.g., as specified in url-pattern of a
                     * jsp-property-group.
                     * Force the context's welcome files, which are interpreted
                     * as JSP files (since they match the url-pattern), to be
                     * considered. See Bugzilla 27664.
                     */
                    mappingData.wrapper = null;
                    checkJspWelcomeFiles = true;
                } else {
                    // See Bugzilla 27704
                    mappingData.wrapperPath.setChars(buf, path.getStart(),
                                                     path.getLength());
                    mappingData.pathInfo.recycle();
                }
            }
        }

        if(mappingData.wrapper == null && noServletPath) {
            // The path is empty, redirect to "/"
            mappingData.redirectPath.setChars
                (path.getBuffer(), pathOffset, pathEnd-pathOffset);
            path.setEnd(pathEnd - 1);
            return;
        }

        // Rule 3 -- Extension Match
        MappedWrapper[] extensionWrappers = contextVersion.extensionWrappers;
        if (mappingData.wrapper == null && !checkJspWelcomeFiles) {
            internalMapExtensionWrapper(extensionWrappers, path, mappingData,
                    true);
        }

        // Rule 4 -- Welcome resources processing for servlets
        if (mappingData.wrapper == null) {
            boolean checkWelcomeFiles = checkJspWelcomeFiles;
            if (!checkWelcomeFiles) {
                char[] buf = path.getBuffer();
                checkWelcomeFiles = (buf[pathEnd - 1] == '/');
            }
            if (checkWelcomeFiles) {
                for (int i = 0; (i < contextVersion.welcomeResources.length)
                         && (mappingData.wrapper == null); i++) {
                    path.setOffset(pathOffset);
                    path.setEnd(pathEnd);
                    path.append(contextVersion.welcomeResources[i], 0,
                            contextVersion.welcomeResources[i].length());
                    path.setOffset(servletPath);

                    // Rule 4a -- Welcome resources processing for exact macth
                    internalMapExactWrapper(exactWrappers, path, mappingData);

                    // Rule 4b -- Welcome resources processing for prefix match
                    if (mappingData.wrapper == null) {
                        internalMapWildcardWrapper
                            (wildcardWrappers, contextVersion.nesting,
                             path, mappingData);
                    }

                    // Rule 4c -- Welcome resources processing
                    //            for physical folder
                    if (mappingData.wrapper == null
                        && contextVersion.resources != null) {
                        String pathStr = path.toString();
                        WebResource file =
                                contextVersion.resources.getResource(pathStr);
                        if (file != null && file.isFile()) {
                            internalMapExtensionWrapper(extensionWrappers, path,
                                                        mappingData, true);
                            if (mappingData.wrapper == null
                                && contextVersion.defaultWrapper != null) {
                                mappingData.wrapper =
                                    contextVersion.defaultWrapper.object;
                                mappingData.requestPath.setChars
                                    (path.getBuffer(), path.getStart(),
                                     path.getLength());
                                mappingData.wrapperPath.setChars
                                    (path.getBuffer(), path.getStart(),
                                     path.getLength());
                                mappingData.requestPath.setString(pathStr);
                                mappingData.wrapperPath.setString(pathStr);
                            }
                        }
                    }
                }

                path.setOffset(servletPath);
                path.setEnd(pathEnd);
            }

        }

        /* welcome file processing - take 2
         * Now that we have looked for welcome files with a physical
         * backing, now look for an extension mapping listed
         * but may not have a physical backing to it. This is for
         * the case of index.jsf, index.do, etc.
         * A watered down version of rule 4
         */
        if (mappingData.wrapper == null) {
            boolean checkWelcomeFiles = checkJspWelcomeFiles;
            if (!checkWelcomeFiles) {
                char[] buf = path.getBuffer();
                checkWelcomeFiles = (buf[pathEnd - 1] == '/');
            }
            if (checkWelcomeFiles) {
                for (int i = 0; (i < contextVersion.welcomeResources.length)
                         && (mappingData.wrapper == null); i++) {
                    path.setOffset(pathOffset);
                    path.setEnd(pathEnd);
                    path.append(contextVersion.welcomeResources[i], 0,
                                contextVersion.welcomeResources[i].length());
                    path.setOffset(servletPath);
                    internalMapExtensionWrapper(extensionWrappers, path,
                                                mappingData, false);
                }

                path.setOffset(servletPath);
                path.setEnd(pathEnd);
            }
        }


        // Rule 7 -- Default servlet
        if (mappingData.wrapper == null && !checkJspWelcomeFiles) {
            if (contextVersion.defaultWrapper != null) {
                mappingData.wrapper = contextVersion.defaultWrapper.object;
                mappingData.requestPath.setChars
                    (path.getBuffer(), path.getStart(), path.getLength());
                mappingData.wrapperPath.setChars
                    (path.getBuffer(), path.getStart(), path.getLength());
            }
            // Redirection to a folder
            char[] buf = path.getBuffer();
            if (contextVersion.resources != null && buf[pathEnd -1 ] != '/') {
                String pathStr = path.toString();
                WebResource file =
                        contextVersion.resources.getResource(pathStr);
                if (file != null && file.isDirectory()) {
                    // Note: this mutates the path: do not do any processing
                    // after this (since we set the redirectPath, there
                    // shouldn't be any)
                    path.setOffset(pathOffset);
                    path.append('/');
                    mappingData.redirectPath.setChars
                        (path.getBuffer(), path.getStart(), path.getLength());
                } else {
                    mappingData.requestPath.setString(pathStr);
                    mappingData.wrapperPath.setString(pathStr);
                }
            }
        }

        path.setOffset(pathOffset);
        path.setEnd(pathEnd);

    }


    /**
     * Exact mapping.
     */
    private final void internalMapExactWrapper
        (MappedWrapper[] wrappers, CharChunk path, MappingData mappingData) {
        int pos = find(wrappers, path);
        if ((pos != -1) && (path.equals(wrappers[pos].name))) {
            mappingData.requestPath.setString(wrappers[pos].name);
            mappingData.wrapper = wrappers[pos].object;
            if (path.equals("/")) {
                // Special handling for Context Root mapped servlet
                mappingData.pathInfo.setString("/");
                mappingData.wrapperPath.setString("");
                // This seems wrong but it is what the spec says...
                mappingData.contextPath.setString("");
            } else {
                mappingData.wrapperPath.setString(wrappers[pos].name);
            }
        }
    }


    /**
     * Wildcard mapping.
     */
    private final void internalMapWildcardWrapper
        (MappedWrapper[] wrappers, int nesting, CharChunk path,
         MappingData mappingData) {

        int pathEnd = path.getEnd();

        int lastSlash = -1;
        int length = -1;
        int pos = find(wrappers, path);
        if (pos != -1) {
            boolean found = false;
            while (pos >= 0) {
                if (path.startsWith(wrappers[pos].name)) {
                    length = wrappers[pos].name.length();
                    if (path.getLength() == length) {
                        found = true;
                        break;
                    } else if (path.startsWithIgnoreCase("/", length)) {
                        found = true;
                        break;
                    }
                }
                if (lastSlash == -1) {
                    lastSlash = nthSlash(path, nesting + 1);
                } else {
                    lastSlash = lastSlash(path);
                }
                path.setEnd(lastSlash);
                pos = find(wrappers, path);
            }
            path.setEnd(pathEnd);
            if (found) {
                mappingData.wrapperPath.setString(wrappers[pos].name);
                if (path.getLength() > length) {
                    mappingData.pathInfo.setChars
                        (path.getBuffer(),
                         path.getOffset() + length,
                         path.getLength() - length);
                }
                mappingData.requestPath.setChars
                    (path.getBuffer(), path.getOffset(), path.getLength());
                mappingData.wrapper = wrappers[pos].object;
                mappingData.jspWildCard = wrappers[pos].jspWildCard;
            }
        }
    }


    /**
     * Extension mappings.
     *
     * @param wrappers          Set of wrappers to check for matches
     * @param path              Path to map
     * @param mappingData       Mapping data for result
     * @param resourceExpected  Is this mapping expecting to find a resource
     */
    private final void internalMapExtensionWrapper(MappedWrapper[] wrappers,
            CharChunk path, MappingData mappingData, boolean resourceExpected) {
        char[] buf = path.getBuffer();
        int pathEnd = path.getEnd();
        int servletPath = path.getOffset();
        int slash = -1;
        for (int i = pathEnd - 1; i >= servletPath; i--) {
            if (buf[i] == '/') {
                slash = i;
                break;
            }
        }
        if (slash >= 0) {
            int period = -1;
            for (int i = pathEnd - 1; i > slash; i--) {
                if (buf[i] == '.') {
                    period = i;
                    break;
                }
            }
            if (period >= 0) {
                path.setOffset(period + 1);
                path.setEnd(pathEnd);
                int pos = find(wrappers, path);
                if ((pos != -1) && (path.equals(wrappers[pos].name)) &&
                        (resourceExpected || !wrappers[pos].resourceOnly)) {
                    mappingData.wrapperPath.setChars
                        (buf, servletPath, pathEnd - servletPath);
                    mappingData.requestPath.setChars
                        (buf, servletPath, pathEnd - servletPath);
                    mappingData.wrapper = wrappers[pos].object;
                }
                path.setOffset(servletPath);
                path.setEnd(pathEnd);
            }
        }
    }


    /**
     * Find a map element given its name in a sorted array of map elements.
     * This will return the index for the closest inferior or equal item in the
     * given array.
     */
    private static final <T> int find(MapElement<T>[] map, CharChunk name) {
        return find(map, name, name.getStart(), name.getEnd());
    }


    /**
     * Find a map element given its name in a sorted array of map elements.
     * This will return the index for the closest inferior or equal item in the
     * given array.
     */
    private static final <T> int find(MapElement<T>[] map, CharChunk name,
                                  int start, int end) {

        int a = 0;
        int b = map.length - 1;

        // Special cases: -1 and 0
        if (b == -1) {
            return -1;
        }

        if (compare(name, start, end, map[0].name) < 0 ) {
            return -1;
        }
        if (b == 0) {
            return 0;
        }

        int i = 0;
        while (true) {
            i = (b + a) / 2;
            int result = compare(name, start, end, map[i].name);
            if (result == 1) {
                a = i;
            } else if (result == 0) {
                return i;
            } else {
                b = i;
            }
            if ((b - a) == 1) {
                int result2 = compare(name, start, end, map[b].name);
                if (result2 < 0) {
                    return a;
                } else {
                    return b;
                }
            }
        }

    }

    /**
     * Find a map element given its name in a sorted array of map elements.
     * This will return the index for the closest inferior or equal item in the
     * given array.
     */
    private static final <T> int findIgnoreCase(MapElement<T>[] map, CharChunk name) {
        return findIgnoreCase(map, name, name.getStart(), name.getEnd());
    }


    /**
     * Find a map element given its name in a sorted array of map elements.
     * This will return the index for the closest inferior or equal item in the
     * given array.
     */
    private static final <T> int findIgnoreCase(MapElement<T>[] map, CharChunk name,
                                  int start, int end) {

        int a = 0;
        int b = map.length - 1;

        // Special cases: -1 and 0
        if (b == -1) {
            return -1;
        }
        if (compareIgnoreCase(name, start, end, map[0].name) < 0 ) {
            return -1;
        }
        if (b == 0) {
            return 0;
        }

        int i = 0;
        while (true) {
            i = (b + a) / 2;
            int result = compareIgnoreCase(name, start, end, map[i].name);
            if (result == 1) {
                a = i;
            } else if (result == 0) {
                return i;
            } else {
                b = i;
            }
            if ((b - a) == 1) {
                int result2 = compareIgnoreCase(name, start, end, map[b].name);
                if (result2 < 0) {
                    return a;
                } else {
                    return b;
                }
            }
        }

    }


    /**
     * Find a map element given its name in a sorted array of map elements.
     * This will return the index for the closest inferior or equal item in the
     * given array.
     */
    private static final <T> int find(MapElement<T>[] map, String name) {

        int a = 0;
        int b = map.length - 1;

        // Special cases: -1 and 0
        if (b == -1) {//数组为空 返回-1
            return -1;
        }

        if (name.compareTo(map[0].name) < 0) {//如果比第一个还小则返回-1
            return -1;
        }
        if (b == 0) {//只有一个数据，并且新的name不比现在的小
            return 0;
        }

        //基于字典序排序的二分查找
        int i = 0;
        while (true) {
            i = (b + a) / 2;
            int result = name.compareTo(map[i].name);
            if (result > 0) {
                a = i;
            } else if (result == 0) {
                return i;
            } else {
                b = i;
            }
            if ((b - a) == 1) {
                int result2 = name.compareTo(map[b].name);
                if (result2 < 0) {
                    return a;
                } else {
                    return b;
                }
            }
        }

    }


    /**
     * Compare given char chunk with String.
     * Return -1, 0 or +1 if inferior, equal, or superior to the String.
     */
    private static final int compare(CharChunk name, int start, int end,
                                     String compareTo) {
        int result = 0;
        char[] c = name.getBuffer();
        int len = compareTo.length();
        if ((end - start) < len) {
            len = end - start;
        }
        for (int i = 0; (i < len) && (result == 0); i++) {
            if (c[i + start] > compareTo.charAt(i)) {
                result = 1;
            } else if (c[i + start] < compareTo.charAt(i)) {
                result = -1;
            }
        }
        if (result == 0) {
            if (compareTo.length() > (end - start)) {
                result = -1;
            } else if (compareTo.length() < (end - start)) {
                result = 1;
            }
        }
        return result;
    }


    /**
     * Compare given char chunk with String ignoring case.
     * Return -1, 0 or +1 if inferior, equal, or superior to the String.
     */
    private static final int compareIgnoreCase(CharChunk name, int start, int end,
                                     String compareTo) {
        int result = 0;
        char[] c = name.getBuffer();
        int len = compareTo.length();
        if ((end - start) < len) {
            len = end - start;
        }
        for (int i = 0; (i < len) && (result == 0); i++) {
            if (Ascii.toLower(c[i + start]) > Ascii.toLower(compareTo.charAt(i))) {
                result = 1;
            } else if (Ascii.toLower(c[i + start]) < Ascii.toLower(compareTo.charAt(i))) {
                result = -1;
            }
        }
        if (result == 0) {
            if (compareTo.length() > (end - start)) {
                result = -1;
            } else if (compareTo.length() < (end - start)) {
                result = 1;
            }
        }
        return result;
    }


    /**
     * Find the position of the last slash in the given char chunk.
     */
    private static final int lastSlash(CharChunk name) {

        char[] c = name.getBuffer();
        int end = name.getEnd();
        int start = name.getStart();
        int pos = end;

        while (pos > start) {
            if (c[--pos] == '/') {
                break;
            }
        }

        return (pos);

    }


    /**
     * Find the position of the nth slash, in the given char chunk.
     */
    private static final int nthSlash(CharChunk name, int n) {

        char[] c = name.getBuffer();
        int end = name.getEnd();
        int start = name.getStart();
        int pos = start;
        int count = 0;

        while (pos < end) {
            if ((c[pos++] == '/') && ((++count) == n)) {
                pos--;
                break;
            }
        }

        return (pos);

    }


    /**
     * Return the slash count in a given string.
     */
    private static final int slashCount(String name) {
        int pos = -1;
        int count = 0;
        while ((pos = name.indexOf('/', pos + 1)) != -1) {
            count++;
        }
        return count;
    }


    /**
     * Insert into the right place in a sorted MapElement array, and prevent
     * duplicates.
     */
    //将oldmap里面的信息保存到newMap，并且还要加入newElement，这里还要基于name进行排序
    private static final <T> boolean insertMap
        (MapElement<T>[] oldMap, MapElement<T>[] newMap, MapElement<T> newElement) {
        int pos = find(oldMap, newElement.name);//找到要newElement 要存放的位置
        if ((pos != -1) && (newElement.name.equals(oldMap[pos].name))) {
            return false;//如果已经存在就表示插入失败
        }
        System.arraycopy(oldMap, 0, newMap, 0, pos + 1);//copy前半段
        newMap[pos + 1] = newElement;//设置newElement
        System.arraycopy
            (oldMap, pos + 1, newMap, pos + 2, oldMap.length - pos - 1);//copy后半段
        return true;
    }


    /**
     * Insert into the right place in a sorted MapElement array.
     */
    private static final <T> boolean removeMap
        (MapElement<T>[] oldMap, MapElement<T>[] newMap, String name) {
        int pos = find(oldMap, name);
        if ((pos != -1) && (name.equals(oldMap[pos].name))) {
            System.arraycopy(oldMap, 0, newMap, 0, pos);
            System.arraycopy(oldMap, pos + 1, newMap, pos,
                             oldMap.length - pos - 1);
            return true;
        }
        return false;
    }


    // ------------------------------------------------- MapElement Inner Class



    protected abstract static class MapElement<T> {

        public String name = null;//名字
        public T object = null;//对应的对象，例如是host对象，context对象或者wrapper对象啥的

    }


    // ------------------------------------------------------- Host Inner Class


    ////对host的map信息
    protected static final class MappedHost
        extends MapElement<Host> {

        public ContextList contextList = null;////有一个contextlist

    }


    // ------------------------------------------------ ContextList Inner Class


    //在mappedhost里面将会用其来存拥有的context的信息
    protected static final class ContextList {

        public MappedContext[] contexts = new MappedContext[0]; //mappedcontext对象的数组
        public int nesting = 0; //所有的context的path中

    }


    // ---------------------------------------------------- Context Inner Class

    ////对context的map的信息
    protected static final class MappedContext extends MapElement<Context> {
        public ContextVersion[] versions = new ContextVersion[0];//版本的数组
    }


    ////某个context的某个版本的具体信息
    protected static final class ContextVersion extends MapElement<Context> {
        public String path = null;//路径
        public String[] welcomeResources = new String[0];
        public WebResourceRoot resources = null;//操作当前web应用程序的资源
        public MappedWrapper defaultWrapper = null;//默认的wrapper
        public MappedWrapper[] exactWrappers = new MappedWrapper[0];//对wrapper的精确的map
        public MappedWrapper[] wildcardWrappers = new MappedWrapper[0];//基于通配符的map
        public MappedWrapper[] extensionWrappers = new MappedWrapper[0];//基于扩展名的map
        public int nesting = 0;

    }


    // ---------------------------------------------------- Wrapper Inner Class

   ///对wrapper对象的map信息
    protected static class MappedWrapper
        extends MapElement<Wrapper> {

        public boolean jspWildCard = false;
        public boolean resourceOnly = false;
    }
}
